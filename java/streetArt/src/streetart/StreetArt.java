/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart;

import java.util.Locale;

/**
 *
 * @author prive
 */
public class StreetArt {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //pour les float avec des points plutôt que des virgules
        Locale.setDefault(Locale.US);
        
        
        Controleur m = new Controleur();
        m.charger();
        int classe=0;
        while (classe!=4) {            
            classe = m.afficherMenu();
            if(classe!=4){
                
                int action = m.afficherSousMenu(classe);
                //actions sur les classes
                switch(action){
                    case 1: m.afficher(classe);break;
                    case 2: m.ajouter(classe);break;
                    case 3: m.supprimer(classe);break;
                }
            }
        }
    }
 
    
    
}
