/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart;

import streetart.metier.Auteur;
import streetart.metier.Photo;
import streetart.metier.Emplacement;
import streetart.metier.Parcours;
import streetart.metier.Fresque;
import streetart.bdd.AuteurBDD;
import streetart.bdd.FresqueBDD;
import streetart.bdd.ParcoursBDD;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author prive
 */
public class Controleur {

    private ArrayList<Auteur> lesAuteurs = new ArrayList<>();
    private ArrayList<Fresque> lesFresques = new ArrayList<>();
    private ArrayList<Parcours> lesParcours = new ArrayList<>();

    public int afficherMenu() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Menu de l'application");
        System.out.println("1- Auteurs");
        System.out.println("2- Fresque");
        System.out.println("3- Parcours");
        System.out.println("4- Quitter");
        int choix = 0;
        while (choix < 1 || choix > 4) {
            try {
                choix = sc.nextInt();
            } catch (InputMismatchException e) {
                choix = 0;
                sc.next();
            }
            if (choix < 1 || choix > 4) {
                System.out.println("Merci de saisir un nombre entre 1 et 4");
            }
        }

        return choix;
    }

    public int afficherSousMenu(int choix) {
        Scanner sc = new Scanner(System.in);
        String item = "";
        switch (choix) {
            case 1:
                item = "auteur";
                break;
            case 2:
                item = "fresque";
                break;
            case 3:
                item = "parcours";
                break;
            case 4:
                return 4;
            default:
                item = "!!erreur";
                break;
        }
        System.out.println("Quelle action sur : " + item);
        System.out.println("1- Afficher " + item);
        System.out.println("2- Ajouter " + item);
        System.out.println("3- Supprimer " + item);

        choix = 0;
        while (choix < 1 || choix > 3) {
            try {
                choix = sc.nextInt();
            } catch (InputMismatchException e) {
                choix = 0;
                sc.next();
            }
            if (choix < 1 || choix > 3) {
                System.out.println("Merci de saisir un nombre entre 1 et 3");
            }
        }

        return choix;
    }

    public void afficher(int classe) {
        switch (classe) {
            case 1:
                for (int i = 0; i < lesAuteurs.size(); i++) {
                    System.out.println(lesAuteurs.get(i).toString());

                }
                break;
            case 2:
                for (int i = 0; i < lesFresques.size(); i++) {
                    System.out.println(lesFresques.get(i).toString());

                }
                break;
            case 3:
                for (int i = 0; i < lesParcours.size(); i++) {
                    System.out.println(lesParcours.get(i).toString());

                }
                break;
            default:
                break;
        }
    }

    public void ajouter(int classe) {
        Scanner sc = new Scanner(System.in);

            switch (classe) {
                case 1: ///// Les auteurs
                    //récup dernier id de la liste
                    int idA = lesAuteurs.get(lesAuteurs.size()-1).getId() +1;
                    System.out.println("Saisir nom auteur");
                    String nom = sc.nextLine();
                    System.out.println("Saisir prenom auteur");
                    String prenom = sc.nextLine();
                    System.out.println("Saisir nom artiste auteur");
                    String artiste = sc.nextLine();
                    System.out.println("Saisir date naissance auteur");
                    String dn = sc.nextLine();
                    System.out.println("Saisir nationalite auteur");
                    String nationalite = sc.nextLine();
                    Auteur a = new Auteur(idA,nom, prenom, artiste, dn, nationalite);
                    lesAuteurs.add(a);
                    //sauvegarde en BDD
                    new AuteurBDD().saveAuteur(a);
                    break;
                case 2:  /// Les fresques
                    //récup dernier id de la liste
                    int idF = lesFresques.get(lesFresques.size()-1).getId() +1;
                    System.out.println("Saisir nom fresque");
                    String nomF = sc.nextLine();
                    System.out.println("Saisir description fresque");
                    String description = sc.nextLine();
                    //emplacement
                    System.out.println("Saisir adresse emplacement");
                    String adresse = sc.nextLine();
                    System.out.println("Saisir ville");
                    String ville = sc.nextLine();
                    System.out.println("Saisir lattitude");
                    Float lattitude = sc.nextFloat();
                    System.out.println("Saisir longitude");
                    Float longitude = sc.nextFloat();
                    //photo
                    System.out.println("Saisir nom photo");
                    String nomP = sc.nextLine();
                    //auteur
                    System.out.println("Quel est l'auteur de la fresque ?");
                    for (int i = 0; i < lesAuteurs.size(); i++) {
                        System.out.println(i + "- " + lesAuteurs.get(i));
                    }
                    Fresque f =new Fresque(idF,nomF, description, new Photo(nomP), lesAuteurs.get(sc.nextInt()), new Emplacement(adresse, ville, lattitude, longitude));
                    lesFresques.add(f);
                    //sauvegarde en BDD
                    new FresqueBDD().saveFresque(f);
                    break;
                case 3: /// Les parcours
                    //récup dernier id de la liste
                    int idP = lesParcours.get(lesParcours.size()-1).getId() +1;
                    System.out.println("Saisir nom parcours");
                    String nomParcours = sc.nextLine();
                    System.out.println("Saisir distance parcours en km");
                    Float distance = sc.nextFloat();
                    ArrayList<Fresque> lesF = new ArrayList<>();
                    System.out.println("Ajouter une fresque sur le parcours / -1 pour quitter");
                    int choix = -2;
                    while (choix != -1) {
                        for (int i = 0; i < lesFresques.size(); i++) {
                            System.out.println(i + "- " + lesFresques.get(i));
                        }
                        choix = sc.nextInt();
                        if (choix != -1) {
                            lesF.add(lesFresques.get(choix));
                            System.out.println("Ajouter une fresque sur le parcours / -1 pour quitter");
                        }
                    }
                    Parcours p = new Parcours(idP,nomParcours, distance,lesF);
                    lesParcours.add(p);   
                    //sauvegarde en BDD
                    new ParcoursBDD().saveParcours(p);
                    
                    break;
            }
      

    }

    public void supprimer(int classe) {
        Scanner sc = new Scanner(System.in);

        switch (classe) {
            case 1:
                for (int i = 0; i < lesAuteurs.size(); i++) {
                    System.out.println(i+"- "+lesAuteurs.get(i).toString());
                }
                System.out.println("Quel auteur supprimer ?");
                Auteur a = lesAuteurs.get(sc.nextInt());
                new AuteurBDD().removeAuteur(a);
                lesAuteurs.remove(a);
                
                break;
            case 2:
                for (int i = 0; i < lesFresques.size(); i++) {
                    System.out.println(i+"- "+lesFresques.get(i).toString());

                }
                System.out.println("Quelle fresque supprimer ?");
                Fresque f = lesFresques.get(sc.nextInt());
                new FresqueBDD().removeFresque(f);
                lesFresques.remove(f);
                break;
            case 3:
                for (int i = 0; i < lesParcours.size(); i++) {
                    System.out.println(i+"- "+lesParcours.get(i).toString());

                }
                System.out.println("Quel parcours supprimer ?");
                Parcours p = lesParcours.get(sc.nextInt());
                new ParcoursBDD().removeParcours(p);
                lesParcours.remove(p);
                break;
            default:
                break;
        }
        charger();
    }

    public void charger(){
        //charger les auteurs
        lesAuteurs = new AuteurBDD().getAllAuteurs();
        //charger les fresques
        lesFresques = new FresqueBDD().getAllFresques();
        //charger les parcours
        lesParcours = new ParcoursBDD().getAllParcours();
    }
    
}
