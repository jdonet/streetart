/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.metier;

import java.util.ArrayList;

/**
 *
 * @author prive
 */
public class Parcours {
    private Integer id;
    private String nom;
    private float distance;
    private ArrayList<Fresque> lesFresques;

    public Parcours(Integer id, String nom, float distance, ArrayList<Fresque> lesFresques) {
        this.id = id;
        this.nom = nom;
        this.distance = distance;
        this.lesFresques = lesFresques;
    }

    public Integer getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public float getDistance() {
        return distance;
    }

    public ArrayList<Fresque> getLesFresques() {
        return lesFresques;
    }

    @Override
    public String toString() {
        return "Parcours{" + "id=" + id + ", nom=" + nom + ", distance=" + distance + ", lesFresques=" + lesFresques + '}';
    }

    
    
}
