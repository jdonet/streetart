/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.metier;

import java.util.ArrayList;

/**
 *
 * @author prive
 */
public class Auteur {

    private Integer id;
    private String nom;
    private String prenom;
    private String nomArtiste;
    private String dateNaissance;
    private String nationalite;

    public Auteur(Integer id, String nom, String prenom, String nomArtiste, String dateNaissance, String nationalite) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.nomArtiste = nomArtiste;
        this.dateNaissance = dateNaissance;
        this.nationalite = nationalite;
    }

    public Integer getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNomArtiste() {
        return nomArtiste;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    
    @Override
    public String toString() {
        return "Auteur{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", nomArtiste=" + nomArtiste + ", dateNaissance=" + dateNaissance + ", nationalite=" + nationalite + '}';
    }
    

    

    
}
