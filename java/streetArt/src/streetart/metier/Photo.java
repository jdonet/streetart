/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.metier;

/**
 *
 * @author prive
 */
public class Photo {
    private String nom;

    public Photo(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    
    @Override
    public String toString() {
        return "Photo{" + "nom=" + nom + '}';
    }
    
    
    
}
