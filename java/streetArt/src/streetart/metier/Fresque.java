/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.metier;

import java.util.ArrayList;

/**
 *
 * @author prive
 */
public class Fresque {

private Integer id;
private String nom;
private String description;
private Photo laPhoto;
private Auteur lAuteur;
private Emplacement lEmplacement;

    public Fresque(Integer id, String nom, String description, Photo laPhoto, Auteur lAuteur, Emplacement lEmplacement) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.laPhoto = laPhoto;
        this.lAuteur = lAuteur;
        this.lEmplacement = lEmplacement;
    }

    public Integer getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public Photo getLaPhoto() {
        return laPhoto;
    }

    public Auteur getlAuteur() {
        return lAuteur;
    }

    public Emplacement getlEmplacement() {
        return lEmplacement;
    }


    @Override
    public String toString() {
        return "Fresque{" + "id=" + id + ", nom=" + nom + ", description=" + description + ", laPhoto=" + laPhoto + ", lAuteur=" + lAuteur + ", lEmplacement=" + lEmplacement + '}';
    }

    
}
