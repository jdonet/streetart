/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.metier;

/**
 *
 * @author prive
 */
public class Emplacement {

    private String adresse;
    private String ville;
    private float lattitude;
    private float longitude;

    public Emplacement(String adresse, String ville, float lattitude, float longitude) {
        this.adresse = adresse;
        this.ville = ville;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getVille() {
        return ville;
    }

    public float getLattitude() {
        return lattitude;
    }

    public float getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "Emplacement{" + "adresse=" + adresse + ", ville=" + ville + ", lattitude=" + lattitude + ", longitude=" + longitude + '}';
    }
    
    
    
}
