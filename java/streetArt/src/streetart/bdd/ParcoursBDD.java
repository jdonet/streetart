/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.bdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import streetart.metier.*;

/**
 *
 * @author prive
 */
public class ParcoursBDD {
    private Connection conn;

    public ParcoursBDD() {
        this.conn = Connexion.seConnecter();
    }
    public ArrayList<Parcours> getAllParcours(){
        // Déclaration des variables
        Statement stmt; 
        ResultSet  resultats;
        resultats = null;
        
        ArrayList<Fresque> lesFresques = new ArrayList();
        ArrayList<Parcours> lesParcours = new ArrayList();

        //Ecriture de la requête SQL
        String req = "SELECT * FROM parcours INNER JOIN composer ON idParcours=refParcours INNER JOIN fresque ON refFresque=idFresque INNER JOIN auteur ON idAuteur=refAuteur INNER JOIN photo ON idPhoto=refPhoto ORDER BY idParcours";
        try {
                 //Récupérer une déclaration SQL liée à la connexion
                   stmt = conn.createStatement();
                    // Exécution de la requête SQL
                resultats = stmt.executeQuery(req);
        } catch (SQLException e) {
                    System.err.println("Anomalie lors de l'execution de la requête");
        }
        //Parcours des données retournées
        try {
                int idParcours=-1;
                String nomParcours="";
                float distanceParcours = 0;
                boolean encore = resultats.next();
                if(encore){
                    idParcours = resultats.getInt("idParcours");
                    nomParcours = resultats.getString("nomParcours");
                    distanceParcours = resultats.getFloat("distanceParcours");
                }else{
                    return lesParcours;
                }
                //Pour chaque enregistrement
                while (encore) {
                    //création de la photo
                    Photo p = new Photo(resultats.getString("nomPhoto"));
                    //création de l'auteur
                    Auteur a = new Auteur(resultats.getInt("idAuteur"),resultats.getString("nomAuteur"), resultats.getString("prenomAuteur"), resultats.getString("pseudoAuteur"), resultats.getString("dateNaissanceAuteur"), resultats.getString("nationaliteAuteur"));
                    //création de l'emplacement
                    Emplacement e = new Emplacement(resultats.getString("adresseFresque"),resultats.getString("villeFresque"), resultats.getFloat("lattitudeFresque"),resultats.getFloat("longitudeFresque"));
                    //création de la fresque
                    Fresque f = new Fresque(resultats.getInt("idFresque"),resultats.getString("nomFresque"), resultats.getString("descriptionFresque"), p,a,e);
                    
                    //ajout de la collection de fresque au parcours ??
                    if(idParcours == resultats.getInt("idParcours")){ //c'est le même parcours
                        //ajout de la fresque
                        lesFresques.add(f);
                    }else{ //nouveau parcours
                        Parcours unParcours = new Parcours(idParcours, nomParcours, distanceParcours, lesFresques);
                        lesParcours.add(unParcours);
                        lesFresques.clear();
                        //ajout de la fresque
                        lesFresques.add(f);
                        idParcours = resultats.getInt("idParcours");
                        nomParcours = resultats.getString("nomParcours");
                        distanceParcours = resultats.getFloat("distanceParcours");
                    }
                    
                    //Passer à l’enregistrement suivant
                    encore = resultats.next();
                }
                Parcours unParcours = new Parcours(idParcours, nomParcours, distanceParcours, lesFresques);
                lesParcours.add(unParcours);
                
                resultats.close(); //fermer proprement la connexion
        } catch (SQLException e) {
                System.out.println (e.getMessage());
        }

        return lesParcours;
    }
    
    public void saveParcours(Parcours unParcours) {
        // Déclaration des variables
        Statement stmt;
        PreparedStatement pstmt;
        ResultSet resultats;
        resultats = null;
        
        try {
            //Ecriture de la photo
            String req = "INSERT INTO parcours (idParcours, nomParcours,distanceParcours) VALUES (?,?,?);";
            pstmt = conn.prepareStatement(req) ;
            pstmt.setString(1,null );
            pstmt.setString(2,unParcours.getNom());
            pstmt.setFloat(3,unParcours.getDistance());
            
             // Exécution de la requête SQL
            pstmt.execute();
            
            //récupération du dernier id de la photo
            req = "SELECT max(idParcours) FROM parcours";
            int max=0;
            try {
                //Récupérer une déclaration SQL liée à la connexion
                  stmt = conn.createStatement();
                // Exécution de la requête SQL
                resultats = stmt.executeQuery(req);
                boolean encore = resultats.next();
                //Pour chaque enregistrement
                if(encore) {
                    max = resultats.getInt(1);
                }
                        resultats.close(); //fermer proprement la connexion
            } catch (SQLException e) {
                        System.err.println("Anomalie lors de l'execution de la requête"+e.getLocalizedMessage());
            }
            
            //ajout de chaque "composer" pour chaque fresque
            for (int i = 0; i < unParcours.getLesFresques().size(); i++) {
                pstmt = conn.prepareStatement("INSERT INTO `composer` (`refFresque`, `refParcours`) VALUES (? ,?)") ;
                pstmt.setInt(1,unParcours.getLesFresques().get(i).getId());
                pstmt.setInt(2,max);
                
                 // Exécution de la requête SQL
                pstmt.execute();
    
            }
            
            

        } catch (SQLException e) {
            System.err.println("Anomalie lors de l'execution de la requête"+e.getLocalizedMessage());
        }

        
    }
    
    public void removeParcours(Parcours unParcours) {
       // Déclaration des variables
        Statement stmt;
        PreparedStatement pstmt;
        ResultSet resultats;
        
        try {
            //suppression dans parcours
            pstmt = conn.prepareStatement("DELETE FROM parcours WHERE idParcours=?") ;
            pstmt.setInt(1,unParcours.getId());
             // Exécution de la requête SQL
            pstmt.execute();
            
            //Récupérer max
            stmt = conn.createStatement();
            // Exécution de la requête SQL
            resultats = stmt.executeQuery("SELECT max(idParcours) + 1 FROM parcours");
            resultats.next();
            
            pstmt = conn.prepareStatement("ALTER TABLE parcours AUTO_INCREMENT = ?") ;
            pstmt.setInt(1,resultats.getInt(1));
            // Exécution de la requête SQL
            pstmt.execute();

        } catch (SQLException e) {
            System.err.println("Anomalie lors de l'execution de la requête"+e.getLocalizedMessage());
        }
    }
    
}
