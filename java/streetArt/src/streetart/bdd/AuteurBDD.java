/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.bdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import streetart.metier.Auteur;

/**
 *
 * @author prive
 */
public class AuteurBDD {

    private Connection conn;

    public AuteurBDD() {
        this.conn = Connexion.seConnecter();
    }

    public ArrayList<Auteur> getAllAuteurs() {
        // Déclaration des variables
        Statement stmt;
        ResultSet resultats;
        resultats = null;
        //Collection des Auteurs renvoyée
        ArrayList<Auteur> lesAuteurs = new ArrayList();

        //Ecriture de la requête SQL
        String req = "SELECT * FROM auteur ORDER by idAuteur";
        try {
            //Récupérer une déclaration SQL liée à la connexion
            stmt = conn.createStatement();
            // Exécution de la requête SQL
            resultats = stmt.executeQuery(req);
        } catch (SQLException e) {
            System.err.println("Anomalie lors de l'execution de la requête");
        }
        //Parcours des données retournées
        try {
            boolean encore = resultats.next();
            //Pour chaque enregistrement
            while (encore) {
                //création de l'auteur et ajout dans la HashMap    
                lesAuteurs.add(new Auteur(resultats.getInt("idAuteur"), resultats.getString("nomAuteur"), resultats.getString("prenomAuteur"), resultats.getString("pseudoAuteur"), resultats.getString("dateNaissanceAuteur"), resultats.getString("nationaliteAuteur")));
                //Passer à l’enregistrement suivant
                encore = resultats.next();
            }
            resultats.close(); //fermer proprement la connexion
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return lesAuteurs;
    }

    public void saveAuteur(Auteur unAuteur) {
        // Déclaration des variables
        Statement stmt;
        PreparedStatement pstmt;
        ResultSet resultats;
        resultats = null;
        
        try {
            //Ecriture de la requête SQL
            pstmt = conn.prepareStatement("INSERT INTO auteur(idAuteur,nomAuteur,prenomAuteur,pseudoAuteur,dateNaissanceAuteur, nationaliteAuteur) VALUES (? ,?, ?, ?, ?, ?)") ;
            pstmt.setString(1,null);
            pstmt.setString(2,unAuteur.getNom());
            pstmt.setString(3,unAuteur.getPrenom());
            pstmt.setString(4,unAuteur.getNomArtiste());
            pstmt.setString(5,unAuteur.getDateNaissance());
            pstmt.setString(6,unAuteur.getNationalite());

            // Exécution de la requête SQL
            pstmt.execute();

            // récupération de la dernière clef primaire

        } catch (SQLException e) {
            System.err.println("Anomalie lors de l'execution de la requête"+e.getLocalizedMessage());
        }

        
    }
    
    public void removeAuteur(Auteur unAuteur) {
       // Déclaration des variables
        Statement stmt;
        PreparedStatement pstmt;
        ResultSet resultats;
        
        try {
            //Suppression de l'auteur
            pstmt = conn.prepareStatement("DELETE FROM auteur WHERE idAuteur=?") ;
            pstmt.setInt(1,unAuteur.getId());
            pstmt.execute();
            
            //suppression des parcours sans fresque
            stmt = conn.createStatement();
            // Exécution de la requête SQL
            stmt.execute("DELETE FROM parcours WHERE idParcours NOT IN (SELECT distinct(refParcours) FROM composer)");
            
            //Récupérer max(idAuteur) +1
            stmt = conn.createStatement();
            resultats = stmt.executeQuery("SELECT max(idAuteur) + 1 FROM auteur");
            resultats.next();
            
            // Mise à jour de l'auto incrément
            pstmt = conn.prepareStatement("ALTER TABLE auteur AUTO_INCREMENT = ?") ;
            pstmt.setInt(1,resultats.getInt(1));
            pstmt.execute();

        } catch (SQLException e) {
            System.err.println("Anomalie lors de l'execution de la requête"+e.getLocalizedMessage());
        }
    }
}
