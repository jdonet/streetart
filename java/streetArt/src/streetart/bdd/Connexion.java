/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author prive
 */
public class Connexion {
    // Déclaration des variables

    private static String driver = "mysql", serverAdress = "localhost", databaseName = "streetArt", userName = "btssio", userPassword = "btssio";
    // La méthode ci-dessous permet d'obtenir un objet connexion correspondant à la base voulue

    protected static Connection seConnecter() {
        String dataSrc = "jdbc:" + driver + "://" + serverAdress + "/" + databaseName;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dataSrc, userName, userPassword);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        }
        if (connection == null) {
            System.out.println("Failed to make connection!");
        }
        return connection;
    }
}
