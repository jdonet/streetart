/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetart.bdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import streetart.metier.*;

/**
 *
 * @author prive
 */
public class FresqueBDD {
    private Connection conn;

    public FresqueBDD() {
        this.conn = Connexion.seConnecter();
    }
    public ArrayList<Fresque> getAllFresques(){
        // Déclaration des variables
        Statement stmt; 
        ResultSet  resultats;
        resultats = null;
        //Collection des Auteurs renvoyée
        ArrayList<Fresque> lesFresques = new ArrayList();

        //Ecriture de la requête SQL
        String req = "SELECT * FROM fresque INNER JOIN auteur ON idAuteur=refAuteur INNER JOIN photo ON idPhoto=refPhoto ORDER by idAuteur";
        try {
                 //Récupérer une déclaration SQL liée à la connexion
                   stmt = conn.createStatement();
                    // Exécution de la requête SQL
                resultats = stmt.executeQuery(req);
        } catch (SQLException e) {
                    System.err.println("Anomalie lors de l'execution de la requête");
        }
        //Parcours des données retournées
        try {
                boolean encore = resultats.next();
                //Pour chaque enregistrement
                while (encore) {
                    //création de la photo
                    Photo p = new Photo(resultats.getString("nomPhoto"));
                    //création de l'auteur
                    Auteur a = new Auteur(resultats.getInt("idAuteur"),resultats.getString("nomAuteur"), resultats.getString("prenomAuteur"), resultats.getString("pseudoAuteur"), resultats.getString("dateNaissanceAuteur"), resultats.getString("nationaliteAuteur"));
                    //création de l'emplacement
                    Emplacement e = new Emplacement(resultats.getString("adresseFresque"),resultats.getString("villeFresque"), resultats.getFloat("lattitudeFresque"),resultats.getFloat("longitudeFresque"));
                    //création de la fresque
                    Fresque f = new Fresque(resultats.getInt("idFresque"),resultats.getString("nomFresque"), resultats.getString("descriptionFresque"), p,a,e);
                    //ajout de la fresque
                    lesFresques.add(f);
                    //Passer à l’enregistrement suivant
                    encore = resultats.next();
                }
                    resultats.close(); //fermer proprement la connexion
        } catch (SQLException e) {
                System.out.println (e.getMessage());
        }

        return lesFresques;
    }
    
    public void saveFresque(Fresque uneFresque) {
        // Déclaration des variables
        Statement stmt;
        PreparedStatement pstmt;
        ResultSet resultats;
        resultats = null;
        
        try {
            //Ecriture de la photo
            String req = "INSERT INTO photo (idPhoto, nomPhoto) VALUES (?,?);";
            pstmt = conn.prepareStatement(req) ;
            pstmt.setString(1,null );
            pstmt.setString(2,uneFresque.getLaPhoto().getNom());
            
            // Exécution de la requête SQL
            pstmt.execute();
            
            //récupération du dernier id de la photo
            req = "SELECT max(idPhoto) FROM photo";
            int max=0;
            try {
                //Récupérer une déclaration SQL liée à la connexion
                  stmt = conn.createStatement();
                // Exécution de la requête SQL
                resultats = stmt.executeQuery(req);
                boolean encore = resultats.next();
                //Pour chaque enregistrement
                if(encore) {
                    max = resultats.getInt(1);
                }
                        resultats.close(); //fermer proprement la connexion
            } catch (SQLException e) {
                        System.err.println("Anomalie lors de l'execution de la requête"+e.getLocalizedMessage());
            }
            
            
            pstmt = conn.prepareStatement("INSERT INTO `fresque` (`idFresque`, `nomFresque`, `descriptionFresque`, `adresseFresque`, `villeFresque`, `lattitudeFresque`, `longitudeFresque`, `refAuteur`, `refPhoto`) VALUES (? ,?, ?, ?, ?, ?, ?, ?, ?)") ;
            pstmt.setInt(1,uneFresque.getId());
            pstmt.setString(2,uneFresque.getNom());
            pstmt.setString(3,uneFresque.getDescription());
            pstmt.setString(4,uneFresque.getlEmplacement().getAdresse());
            pstmt.setString(5,uneFresque.getlEmplacement().getVille());
            pstmt.setFloat(6,uneFresque.getlEmplacement().getLattitude());
            pstmt.setFloat(7,uneFresque.getlEmplacement().getLongitude());
            pstmt.setInt(8,uneFresque.getlAuteur().getId());
            pstmt.setInt(9,max);

             // Exécution de la requête SQL
            pstmt.execute();

            // récupération de la dernière clef primaire

        } catch (SQLException e) {
            System.err.println("Anomalie lors de l'execution de la requête"+e.getLocalizedMessage());
        }

        
    }
    
    public void removeFresque(Fresque uneFresque) {
       // Déclaration des variables
        Statement stmt;
        PreparedStatement pstmt;
        ResultSet resultats;
        
        try {
            //Ecriture de la requête SQL
            pstmt = conn.prepareStatement("DELETE FROM fresque WHERE idFresque=?") ;
            pstmt.setInt(1,uneFresque.getId());
             // Exécution de la requête SQL
            pstmt.execute();
            
            //suppression des photos orphelines
            stmt = conn.createStatement();
            // Exécution de la requête SQL
            stmt.execute("DELETE FROM photo WHERE idPhoto NOT IN (SELECT distinct(refPhoto) FROM fresque)");
            
            //suppression des parcours sans fresque
            stmt = conn.createStatement();
            // Exécution de la requête SQL
            stmt.execute("DELETE FROM parcours WHERE idParcours NOT IN (SELECT distinct(refParcours) FROM composer)");
            
            //Récupérer max
            stmt = conn.createStatement();
            // Exécution de la requête SQL
            resultats = stmt.executeQuery("SELECT max(idFresque) + 1 FROM fresque");
            resultats.next();
            
            pstmt = conn.prepareStatement("ALTER TABLE fresque AUTO_INCREMENT = ?") ;
            pstmt.setInt(1,resultats.getInt(1));
            // Exécution de la requête SQL
            pstmt.execute();

        } catch (SQLException e) {
            System.err.println("Anomalie lors de l'execution de la requête"+e.getLocalizedMessage());
        }
    }
    
}
