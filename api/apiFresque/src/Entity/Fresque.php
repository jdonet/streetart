<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Fresque
 *
 * @ORM\Table(name="fresque", indexes={@ORM\Index(name="refAuteur", columns={"refAuteur"}), @ORM\Index(name="refPhoto", columns={"refPhoto"})})
 * @ORM\Entity
 */
class Fresque
{
    /**
     * @var int
     *
     * @ORM\Column(name="idFresque", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idfresque;

    /**
     * @var string
     *
     * @ORM\Column(name="nomFresque", type="string", length=255, nullable=false)
     */
    private $nomfresque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptionFresque", type="text", length=65535, nullable=true)
     */
    private $descriptionfresque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresseFresque", type="text", length=65535, nullable=true)
     */
    private $adressefresque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="villeFresque", type="string", length=255, nullable=true)
     */
    private $villefresque;

    /**
     * @var string
     *
     * @ORM\Column(name="lattitudeFresque", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $lattitudefresque = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="longitudeFresque", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $longitudefresque = '0';

    /**
     * @var \Auteur
     *
     * @ORM\ManyToOne(targetEntity="Auteur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refAuteur", referencedColumnName="idAuteur")
     * })
     */
    private $refauteur;

    /**
     * @var \Photo
     *
     * @ORM\ManyToOne(targetEntity="Photo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refPhoto", referencedColumnName="idPhoto")
     * })
     */
    private $refphoto;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Parcours", inversedBy="reffresque")
     * @ORM\JoinTable(name="composer",
     *   joinColumns={
     *     @ORM\JoinColumn(name="refFresque", referencedColumnName="idFresque")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="refParcours", referencedColumnName="idParcours")
     *   }
     * )
     */
    private $refparcours;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refparcours = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdfresque(): ?int
    {
        return $this->idfresque;
    }

    public function getNomfresque(): ?string
    {
        return $this->nomfresque;
    }

    public function setNomfresque(string $nomfresque): self
    {
        $this->nomfresque = $nomfresque;

        return $this;
    }

    public function getDescriptionfresque(): ?string
    {
        return $this->descriptionfresque;
    }

    public function setDescriptionfresque(?string $descriptionfresque): self
    {
        $this->descriptionfresque = $descriptionfresque;

        return $this;
    }

    public function getAdressefresque(): ?string
    {
        return $this->adressefresque;
    }

    public function setAdressefresque(?string $adressefresque): self
    {
        $this->adressefresque = $adressefresque;

        return $this;
    }

    public function getVillefresque(): ?string
    {
        return $this->villefresque;
    }

    public function setVillefresque(?string $villefresque): self
    {
        $this->villefresque = $villefresque;

        return $this;
    }

    public function getLattitudefresque(): ?string
    {
        return $this->lattitudefresque;
    }

    public function setLattitudefresque(string $lattitudefresque): self
    {
        $this->lattitudefresque = $lattitudefresque;

        return $this;
    }

    public function getLongitudefresque(): ?string
    {
        return $this->longitudefresque;
    }

    public function setLongitudefresque(string $longitudefresque): self
    {
        $this->longitudefresque = $longitudefresque;

        return $this;
    }

    public function getRefauteur(): ?Auteur
    {
        return $this->refauteur;
    }

    public function setRefauteur(?Auteur $refauteur): self
    {
        $this->refauteur = $refauteur;

        return $this;
    }

    public function getRefphoto(): ?Photo
    {
        return $this->refphoto;
    }

    public function setRefphoto(?Photo $refphoto): self
    {
        $this->refphoto = $refphoto;

        return $this;
    }

    /**
     * @return Collection|Parcours[]
     */
    public function getRefparcours(): Collection
    {
        return $this->refparcours;
    }

    public function addRefparcour(Parcours $refparcour): self
    {
        if (!$this->refparcours->contains($refparcour)) {
            $this->refparcours[] = $refparcour;
        }

        return $this;
    }

    public function removeRefparcour(Parcours $refparcour): self
    {
        if ($this->refparcours->contains($refparcour)) {
            $this->refparcours->removeElement($refparcour);
        }

        return $this;
    }

}
