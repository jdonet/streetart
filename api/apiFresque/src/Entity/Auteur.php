<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Auteur
 *
 * @ORM\Table(name="auteur")
 * @ORM\Entity
 */
class Auteur
{
    /**
     * @var int
     *
     * @ORM\Column(name="idAuteur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idauteur;

    /**
     * @var string
     *
     * @ORM\Column(name="nomAuteur", type="string", length=255, nullable=false)
     */
    private $nomauteur;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomAuteur", type="string", length=255, nullable=false)
     */
    private $prenomauteur;

    /**
     * @var string
     *
     * @ORM\Column(name="pseudoAuteur", type="string", length=255, nullable=false)
     */
    private $pseudoauteur;

    /**
     * @var string
     *
     * @ORM\Column(name="dateNaissanceAuteur", type="string", length=255, nullable=false)
     */
    private $datenaissanceauteur;

    /**
     * @var string
     *
     * @ORM\Column(name="nationaliteAuteur", type="text", length=65535, nullable=false)
     */
    private $nationaliteauteur;

    public function getIdauteur(): ?int
    {
        return $this->idauteur;
    }

    public function getNomauteur(): ?string
    {
        return $this->nomauteur;
    }

    public function setNomauteur(string $nomauteur): self
    {
        $this->nomauteur = $nomauteur;

        return $this;
    }

    public function getPrenomauteur(): ?string
    {
        return $this->prenomauteur;
    }

    public function setPrenomauteur(string $prenomauteur): self
    {
        $this->prenomauteur = $prenomauteur;

        return $this;
    }

    public function getPseudoauteur(): ?string
    {
        return $this->pseudoauteur;
    }

    public function setPseudoauteur(string $pseudoauteur): self
    {
        $this->pseudoauteur = $pseudoauteur;

        return $this;
    }

    public function getDatenaissanceauteur(): ?string
    {
        return $this->datenaissanceauteur;
    }

    public function setDatenaissanceauteur(string $datenaissanceauteur): self
    {
        $this->datenaissanceauteur = $datenaissanceauteur;

        return $this;
    }

    public function getNationaliteauteur(): ?string
    {
        return $this->nationaliteauteur;
    }

    public function setNationaliteauteur(string $nationaliteauteur): self
    {
        $this->nationaliteauteur = $nationaliteauteur;

        return $this;
    }


}
