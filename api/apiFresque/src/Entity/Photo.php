<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Photo
 *
 * @ORM\Table(name="photo")
 * @ORM\Entity
 */
class Photo
{
    /**
     * @var int
     *
     * @ORM\Column(name="idPhoto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idphoto;

    /**
     * @var string
     *
     * @ORM\Column(name="nomPhoto", type="text", length=65535, nullable=false)
     */
    private $nomphoto;

    public function getIdphoto(): ?int
    {
        return $this->idphoto;
    }

    public function getNomphoto(): ?string
    {
        return $this->nomphoto;
    }

    public function setNomphoto(string $nomphoto): self
    {
        $this->nomphoto = $nomphoto;

        return $this;
    }


}
