<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Parcours
 *
 * @ORM\Table(name="parcours")
 * @ORM\Entity
 */
class Parcours
{
    /**
     * @var int
     *
     * @ORM\Column(name="idParcours", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idparcours;

    /**
     * @var string
     *
     * @ORM\Column(name="nomParcours", type="string", length=255, nullable=false)
     */
    private $nomparcours;

    /**
     * @var string
     *
     * @ORM\Column(name="distanceParcours", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $distanceparcours;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Fresque", mappedBy="refparcours")
     */
    private $reffresque;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reffresque = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdparcours(): ?int
    {
        return $this->idparcours;
    }

    public function getNomparcours(): ?string
    {
        return $this->nomparcours;
    }

    public function setNomparcours(string $nomparcours): self
    {
        $this->nomparcours = $nomparcours;

        return $this;
    }

    public function getDistanceparcours(): ?string
    {
        return $this->distanceparcours;
    }

    public function setDistanceparcours(string $distanceparcours): self
    {
        $this->distanceparcours = $distanceparcours;

        return $this;
    }

    /**
     * @return Collection|Fresque[]
     */
    public function getReffresque(): Collection
    {
        return $this->reffresque;
    }

    public function addReffresque(Fresque $reffresque): self
    {
        if (!$this->reffresque->contains($reffresque)) {
            $this->reffresque[] = $reffresque;
            $reffresque->addRefparcour($this);
        }

        return $this;
    }

    public function removeReffresque(Fresque $reffresque): self
    {
        if ($this->reffresque->contains($reffresque)) {
            $this->reffresque->removeElement($reffresque);
            $reffresque->removeRefparcour($this);
        }

        return $this;
    }

}
