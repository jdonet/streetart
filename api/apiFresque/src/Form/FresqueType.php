<?php

namespace App\Form;

use App\Entity\Fresque;
use App\Entity\Auteur;
use App\Entity\Photo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FresqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomfresque')
            ->add('descriptionfresque')
            ->add('adressefresque')
            ->add('villefresque')
            ->add('lattitudefresque')
            ->add('longitudefresque')
            ->add('refauteur', EntityType::class, [
                // looks for choices from this entity
                'class' => Auteur::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'nomauteur',
            
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('refphoto', EntityType::class, [
                // looks for choices from this entity
                'class' => Photo::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'nomphoto',
            
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ]);
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fresque::class,
        ]);
    }
}
