<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200229210452 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, api_token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6497BA2F5EB (api_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fresque DROP FOREIGN KEY fresque_ibfk_1');
        $this->addSql('ALTER TABLE fresque DROP FOREIGN KEY fresque_ibfk_2');
        $this->addSql('ALTER TABLE fresque CHANGE descriptionFresque descriptionFresque TEXT DEFAULT NULL, CHANGE adresseFresque adresseFresque TEXT DEFAULT NULL, CHANGE villeFresque villeFresque VARCHAR(255) DEFAULT NULL, CHANGE refAuteur refAuteur INT DEFAULT NULL, CHANGE refPhoto refPhoto INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fresque ADD CONSTRAINT FK_211BE727FB053D28 FOREIGN KEY (refAuteur) REFERENCES auteur (idAuteur)');
        $this->addSql('ALTER TABLE fresque ADD CONSTRAINT FK_211BE7279F209A80 FOREIGN KEY (refPhoto) REFERENCES photo (idPhoto)');
        $this->addSql('ALTER TABLE composer DROP FOREIGN KEY composer_ibfk_1');
        $this->addSql('ALTER TABLE composer ADD CONSTRAINT FK_987306D8628C51C1 FOREIGN KEY (refFresque) REFERENCES fresque (idFresque)');
        $this->addSql('ALTER TABLE composer RENAME INDEX refparcours TO IDX_987306D8D09B0E18');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE composer DROP FOREIGN KEY FK_987306D8628C51C1');
        $this->addSql('ALTER TABLE composer ADD CONSTRAINT composer_ibfk_1 FOREIGN KEY (refFresque) REFERENCES fresque (idFresque) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE composer RENAME INDEX idx_987306d8d09b0e18 TO refParcours');
        $this->addSql('ALTER TABLE fresque DROP FOREIGN KEY FK_211BE727FB053D28');
        $this->addSql('ALTER TABLE fresque DROP FOREIGN KEY FK_211BE7279F209A80');
        $this->addSql('ALTER TABLE fresque CHANGE descriptionFresque descriptionFresque TEXT CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE adresseFresque adresseFresque TEXT CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE villeFresque villeFresque VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE refAuteur refAuteur INT NOT NULL, CHANGE refPhoto refPhoto INT NOT NULL');
        $this->addSql('ALTER TABLE fresque ADD CONSTRAINT fresque_ibfk_1 FOREIGN KEY (refAuteur) REFERENCES auteur (idAuteur) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fresque ADD CONSTRAINT fresque_ibfk_2 FOREIGN KEY (refPhoto) REFERENCES photo (idPhoto) ON UPDATE CASCADE ON DELETE CASCADE');
    }
}
