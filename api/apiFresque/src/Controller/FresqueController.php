<?php

namespace App\Controller;

use App\Entity\Fresque;
use App\Form\FresqueType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/fresque")
 */
class FresqueController extends AbstractController
{
    /**
     * @Route("/", name="fresque_index", methods={"GET"})
     */
    public function index(): Response
    {
        $fresques = $this->getDoctrine()
            ->getRepository(Fresque::class)
            ->findAll();

        return $this->render('fresque/index.html.twig', [
            'fresques' => $fresques,
        ]);
    }

    /**
     * @Route("/new", name="fresque_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $fresque = new Fresque();
        $form = $this->createForm(FresqueType::class, $fresque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fresque);
            $entityManager->flush();

            return $this->redirectToRoute('fresque_index');
        }

        return $this->render('fresque/new.html.twig', [
            'fresque' => $fresque,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idfresque}", name="fresque_show", methods={"GET"})
     */
    public function show(Fresque $fresque): Response
    {
        return $this->render('fresque/show.html.twig', [
            'fresque' => $fresque,
        ]);
    }

    /**
     * @Route("/{idfresque}/edit", name="fresque_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Fresque $fresque): Response
    {
        $form = $this->createForm(FresqueType::class, $fresque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('fresque_index');
        }

        return $this->render('fresque/edit.html.twig', [
            'fresque' => $fresque,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idfresque}", name="fresque_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Fresque $fresque): Response
    {
        if ($this->isCsrfTokenValid('delete'.$fresque->getIdfresque(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($fresque);
            $entityManager->flush();
        }

        return $this->redirectToRoute('fresque_index');
    }
}
