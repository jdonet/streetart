<?php
// src/Controller/MyController.php
namespace App\Controller;

use App\Entity\Fresque;
use App\Entity\Parcours;
use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ConnexionForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MyController extends AbstractController
{
    public function welcome()
    {
        return new Response(
            '<html>
            <body>
            <h1>API gestion fresques et parcours</h1>
            <a href="/lesFresques"> Les fresques </a><br>
            <a href="/lesParcours"> Les parcours </a><br>
            <a href="/logout"> Déconnecter </a>
            </body></html>'
        );
    }
    
    /**
     * @Route("/lesFresques", name="lesFresques", methods={"GET"})
     */
    public function getAllFresques(): Response
    {
            $fresques = $this->getDoctrine()
                ->getRepository(Fresque::class)
                ->findAll();

            return $this->render('fresque/getAll.html.twig', [
                'fresques' => $fresques,
                'nb' => count($fresques),
            ]);
        
    }

    /**
     * @Route("/lesParcours", name="lesParcours", methods={"GET"})
     */
    public function getAllParcours(): Response
    {
            $parcours = $this->getDoctrine()
                ->getRepository(Parcours::class)
                ->findAll();

            return $this->render('parcours/getAll.html.twig', [
                'parcours' => $parcours,
                'nb' => count($parcours),
            ]);
        
    }

    /**
     * @Route("/cnx", name="cnx", methods={"GET"})
     */
    public function connexion(UserPasswordEncoderInterface $encoder){
        //récupération du login en GET
        $request = Request::createFromGlobals();
        $l = $request->query->get('login');
        //select user where email = $l
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $l]);
        //récupération du pass en GET
        $p = $request->query->get('password');
        //vérification si le mot de passe passé en paramètre, une fois encodé est le celui en BDD
        $valid = $encoder->isPasswordValid($user, $p);

        if($valid == 1){//authentification ok
            //génération du token
            $token = $this->randomToken(200);
            //modifier le token du user
            $user->setApiToken($token);
            //svg le token en bdd
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
        
            //renvoyer le token généré
            return new Response(
                '<html>
                <body>
                {"status":1,<br>"token":"'.$user->getApiToken().'"}</body></html>' );
        }else{
            return new Response(
                '<html>
                <body>
                {"status":0,"error":"bad auth"}</body></html>'
            );
        }
    }


    public function randomToken(int $length) {
        if($length>1 && $length<255){
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $token = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < $length; $i++) {
                $n = rand(0, $alphaLength);
                $token[] = $alphabet[$n];
            }
            return implode($token); //turn the array into a string
        }else{
            return null;
        }
    }

     /**
     * @Route("/disconnect", name="disconnect", methods={"GET"})
     */
    public function disconnect(){
        //récupération du token en GET
        $request = Request::createFromGlobals();
        $t = $request->query->get('token');
        //select user where token = $t
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['apiToken' => $t]);
        if($user){
        //modifier le token du user
        $user->setApiToken("");
        //svg le token en bdd
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
    
            //renvoyer le token généré
            return new Response(
                '<html>
                <body>
                {"status":1,<br>"msg":"deconnecte"}</body></html>'
            );
        }else{
            return new Response(
                '<html>
                <body>
                {"status":0,"error":"bad token"}</body></html>'
            );
        }
    }
}
