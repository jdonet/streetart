package com.example.applifresque;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailFresque extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_fresque);

        //récupération des paramètres
        Intent intent = getIntent();
        String nom = intent.getStringExtra("nomFresque");
        String description = intent.getStringExtra("descriptionFresque");
        String photo = intent.getStringExtra("nomPhoto");
        //récupération des composants
        ImageView photoV = findViewById(R.id.photoFresqueView);
        String uri = "@drawable/"+photo;  // where myresource (without the extension) is the file
        int imageResource = getResources().getIdentifier(uri, null, getPackageName());
        photoV.setImageResource(imageResource);
        TextView nomV = findViewById(R.id.nomFresqueView);
        nomV.setText(nom);
        TextView descV = findViewById(R.id.descriptionFresqueView);
        descV.setText(description);

        //récup bouton et ajout action listener
        Button btClose = findViewById(R.id.btClose);
        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
