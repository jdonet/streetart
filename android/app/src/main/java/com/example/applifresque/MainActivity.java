package com.example.applifresque;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements FresqueFragment.OnListFragmentInteractionListener {

    private Controller controller = new Controller(this);

    public Controller getController() {
        return controller;
    }


    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
/*
        //ajout perso
        recyclerView = (RecyclerView) findViewById(R.id.list);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //remplir Fresques
        Controller c = new Controller(this);
        c.recupererLesFresques(new VolleyCallback(){
            @Override
            public void onSuccess(){
                // specify an adapter (see also next example)
                MyFresqueRecyclerViewAdapter mAdapter = new MyFresqueRecyclerViewAdapter(controller.getLesFresques(), new FresqueFragment.OnListFragmentInteractionListener() {
                    @Override
                    public void onListFragmentInteraction(Fresque item) {
                    }
                });
                recyclerView.setAdapter(mAdapter);
            }
        });
*/
    }

    public void ajoutFresquesSurCarte(MapView map, ArrayList<OverlayItem> lesMarker ){
        //the overlay
        ItemizedOverlayWithFocus<OverlayItem> mOverlay = new ItemizedOverlayWithFocus<OverlayItem>(lesMarker,
                new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
                    @Override
                    public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
//récupération de la fresque sur laquelle on a cliqué
                        Fresque f = controller.getLesFresques().get(index);
//création des paramètres à passer à l'activity
                        Intent detailsActivity = new Intent(MainActivity.this,DetailFresque .class);
                        detailsActivity.putExtra("nomFresque",f.getNomFresque());
                        detailsActivity.putExtra("descriptionFresque",f.getDescriptionFresque());
                        detailsActivity.putExtra("nomPhoto",f.getNomPhoto());
                        startActivity(detailsActivity);

                        return true;

                    }
                    @Override
                    public boolean onItemLongPress(final int index, final OverlayItem item) {
                        return false;
                    }
                }, this);
        mOverlay.setFocusItemsOnTap(true);

        map.getOverlays().add(mOverlay);
    }

    @Override
    public void onListFragmentInteraction(Fresque item) {
        //création des paramètres à passer à l'activity
        Intent detailsActivity = new Intent(MainActivity.this,DetailFresque .class);
        detailsActivity.putExtra("nomFresque",item.getNomFresque());
        detailsActivity.putExtra("descriptionFresque",item.getDescriptionFresque());
        detailsActivity.putExtra("nomPhoto",item.getNomPhoto());
        startActivity(detailsActivity);

    }
}
