package com.example.applifresque;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Controller {
    private static ArrayList<Fresque> lesFresques = new ArrayList<>();
    Context context;

    public Controller(Context context) {
        this.context = context;
    }
    public ArrayList<Fresque> getLesFresques() {
        return lesFresques;
    }
    public ArrayList<OverlayItem> getLesMarker(){
        ArrayList<OverlayItem> lesMarker = new ArrayList<OverlayItem>();
        for (Fresque f : lesFresques) {
            lesMarker.add(new OverlayItem(f.getNomFresque(), f.getDescriptionFresque(), new GeoPoint(f.getLattitude(),f.getLongitude()))); // Lat/Lon decimal degrees
        }
        return lesMarker;
    }

    public void recupererLesFresques(final VolleyCallback callback){

        String url = "http://51.75.25.79:8080/lesFresques"; //A adapter si vous voulez vous connecter à votre API!!!

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsonToFresque(response);
                        callback.onSuccess();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("","Erreur !!!!!!!!!"+error);
                    }
                }
                ) {
            //Ajout du header d'authentification
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-AUTH-TOKEN", "monToken");
                return params;
            }
        };
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest,"headerRequest");
    }

    private void parseJsonToFresque(JSONObject json){
        try {
            JSONArray lesFresquesJSON = json.getJSONArray("fresques");
            for (int i=0;i<lesFresquesJSON.length();i++) {
                JSONObject uneFresque = lesFresquesJSON.getJSONObject(i).getJSONObject("fresque");
                lesFresques.add(new Fresque(uneFresque.getInt("id"),uneFresque.getString("nom"),uneFresque.getString("description"), uneFresque.getDouble("lattitude"),uneFresque.getDouble("longitude"),uneFresque.getJSONObject("photo").getString("nom")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("","vraie erreur !!!!!!!!!"+e.getLocalizedMessage());
        }
    }
}
