package com.example.applifresque;

public class Fresque {
    private Integer idFresque;
    private String nomFresque;
    private String descriptionFresque;
    private double lattitude;
    private double longitude;
    private String nomPhoto;

    public Fresque(Integer idFresque, String nomFresque, String descriptionFresque, double lattitude, double longitude, String nomPhoto) {
        this.idFresque = idFresque;
        this.nomFresque = nomFresque;
        this.descriptionFresque = descriptionFresque;
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.nomPhoto = nomPhoto;
    }

    public Integer getIdFresque() {
        return idFresque;
    }

    public String getNomFresque() {
        return nomFresque;
    }

    public String getDescriptionFresque() {
        return descriptionFresque;
    }

    public double getLattitude() {
        return lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getNomPhoto() {
        return nomPhoto;
    }


}
